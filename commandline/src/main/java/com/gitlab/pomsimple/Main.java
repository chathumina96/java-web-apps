package com.gitlab.pomsimple;

import com.gitlab.inject.*;
import com.gitlab.crypto.*;

public class Main {
	public static void main(String[] args) throws Exception {
		TestInjection Test = new TestInjection("https://example.com");

		RsaNoPadding rsa= new RsaNoPadding();
		rsa.rsaCipherOk();
		rsa.rsaCipherWeak();
		rsa.dataflowCipherWeak();
	}
}
